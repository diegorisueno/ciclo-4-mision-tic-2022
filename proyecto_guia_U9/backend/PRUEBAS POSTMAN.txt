//Agregar Usuarios	
//POST
http://localhost:4000/api/usuarios/
//BODY
{
   "nombre": "Edilberto Sierra",
   "email": "misiontic.formador87@uis.edu.co",
   "password": "123456"
}


{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjoiNjM1ZmRlMTg5NzVhOTVjZDhhMGFmMDFmIn0sImlhdCI6MTY2NzIyNzE2MCwiZXhwIjoxNjY3MjMwNzYwfQ.00t6KN3UeB-ptHGELisl48HzcfSDiQch38dWL4hHXKw"
}


//Agregar Proyectos
//POST
http://localhost:4000/api/proyectos/
//HEADER
KEY: x-auth-token
VALUE: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjoiNjM1ZmRlMTg5NzVhOTVjZDhhMGFmMDFmIn0sImlhdCI6MTY2NzIyNzE2MCwiZXhwIjoxNjY3MjMwNzYwfQ.00t6KN3UeB-ptHGELisl48HzcfSDiQch38dWL4hHXKw

//BODY
{
    "nombre": "Proyecto Prueba",
    "creado": "2022-10-31T14:32:15.244Z",
    "_id": "635fe17f975a95cd8a0af021",
    "creador": "635fde18975a95cd8a0af01f"
}


//Actualizar Proyectos
//PUT
http://localhost:4000/api/proyectos/635fe17f975a95cd8a0af021
KEY: x-auth-token
VALUE: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjoiNjM1ZmRlMTg5NzVhOTVjZDhhMGFmMDFmIn0sImlhdCI6MTY2NzIyNzE2MCwiZXhwIjoxNjY3MjMwNzYwfQ.00t6KN3UeB-ptHGELisl48HzcfSDiQch38dWL4hHXKw
//BODY
{
    "nombre": "Proyecto Prueba Actualizado"
}

//Listar Proyectos
//GET
http://localhost:4000/api/proyectos/
KEY: x-auth-token
VALUE: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjoiNjM1ZmRlMTg5NzVhOTVjZDhhMGFmMDFmIn0sImlhdCI6MTY2NzIyNzE2MCwiZXhwIjoxNjY3MjMwNzYwfQ.00t6KN3UeB-ptHGELisl48HzcfSDiQch38dWL4hHXKw

//Cargar un Proyecto
//GET
http://localhost:4000/api/proyectos/635fe17f975a95cd8a0af021
KEY: x-auth-token
VALUE: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjoiNjM1ZmRlMTg5NzVhOTVjZDhhMGFmMDFmIn0sImlhdCI6MTY2NzIyNzE2MCwiZXhwIjoxNjY3MjMwNzYwfQ.00t6KN3UeB-ptHGELisl48HzcfSDiQch38dWL4hHXKw

//Borrar un Proyecto
//DELETE
http://localhost:4000/api/proyectos/635fe17f975a95cd8a0af021
KEY: x-auth-token
VALUE: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjoiNjM1ZmRlMTg5NzVhOTVjZDhhMGFmMDFmIn0sImlhdCI6MTY2NzIyNzE2MCwiZXhwIjoxNjY3MjMwNzYwfQ.00t6KN3UeB-ptHGELisl48HzcfSDiQch38dWL4hHXKw



