const express = require('express');
const router = express.Router();

const controladorProductos = require('./router_productos');
router.use("/productos",controladorProductos);

const controladorProductosSinAuth = require('./router_productos');
router.use("/productossinauth",controladorProductosSinAuth);

const rutaUsuarios = require('./router_usuarios');
router.use("/usuarios",rutaUsuarios);

const rutaAuth = require('./router_auth');
router.use("/auth",rutaAuth);

module.exports = router



