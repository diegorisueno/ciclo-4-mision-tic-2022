//import logo from './logo.svg';
//import './App.css';
import React, {Fragment} from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';

import Login from './componentes/login';
import Register from './componentes/register';

import Inicio from './componentes/inicio';

import ProductosListar from './componentes/productoslistar';
import ProductosBorrar from './componentes/productosborrar';
import ProductosEditar from './componentes/productoseditar';
import ProductosAgregar from './componentes/productosagregar';
import ProductosCombo from './componentes/productoscombo';

//import CategoriasListar from './componentes/categoriaslistar';

function App()
{
  return(
  <main className='App'>
    <Fragment>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Login/>} exact/>
          <Route path='/register' element={<Register/>} exact/>
          <Route path='/inicio' element={<Inicio/>} exact></Route>
          <Route path='/productoslistar' element={<ProductosListar/>} exact></Route>
          <Route path='/productosagregar' element={<ProductosAgregar/>} exact></Route>
          <Route path='/productoseditar/:id' element={<ProductosEditar/>} exact></Route>
          <Route path='/productosborrar/:id' element={<ProductosBorrar/>} exact></Route>
          <Route path='/productoscombo' element={<ProductosCombo/>} exact></Route>
        </Routes>
      </BrowserRouter>
    </Fragment>
  </main>
  )
  
/*
  return (
    <div className="App">
      <nav className="navbar navbar-expand-lg bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">MisionTic</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li key="inicio" className="nav-item">
                <a className="nav-link active" aria-current="page" href="/">Inicio</a>
              </li>
              <li key="productos" className="nav-item">
                <a className="nav-link" href="/productoslistar">Productos</a>
              </li>
              <li key="categorias" className="nav-item">
                <a className="nav-link" href="/categoriaslistar">Categorias</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <BrowserRouter>
        <Routes>
            <Route path='/productoslistar' element={<ProductosListar/>} exact></Route>
            <Route path='/productoseditar/:id' element={<ProductosEditar/>} exact></Route>
            <Route path='/productosborrar/:id' element={<ProductosBorrar/>} exact></Route>
            <Route path='/productosagregar' element={<ProductosAgregar/>} exact></Route>
        </Routes>
      </BrowserRouter>    
    </div>
  );
  */
}

//<Route path='/productosagregar' element={<ProductosAgregar/>} exact></Route>

export default App;
