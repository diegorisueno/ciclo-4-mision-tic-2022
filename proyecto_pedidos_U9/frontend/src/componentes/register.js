import {Link} from 'react-router-dom';

function Register()
{
    return (
    <div className="Register">
	<section className="login common-img-bg">
		<div className="container-fluid">
			<div className="row">
					<div className="col-sm-12">
						<div className="login-card card-block bg-white">
							<form className="md-float-material" action="index.html">
								<div className="text-center">
									<img src="assets/images/logouismisiontic.png" alt="logo"></img>
								</div>
								<h3 className="text-center txt-primary">Crear una cuenta</h3>
                                <div className="md-input-wrapper">
									<input type="text" id="nombre" className="md-form-control" required="required"></input>
									<label>Nombre</label>
								</div>

								<div className="md-input-wrapper">
									<input type="email" id="email" className="md-form-control" required="required"></input>
									<label>Email</label>
								</div>

								<div className="md-input-wrapper">
									<input type="password" id="password" className="md-form-control" required="required"></input>
									<label>Password</label>
								</div>
                                
								<div className="md-input-wrapper">
									<input type="password" id="confirmar" className="md-form-control" required="required"></input>
									<label>Confirm Password</label>
								</div>
								<div className="rkmd-checkbox checkbox-rotate checkbox-ripple b-none m-b-20">
									<label className="input-checkbox checkbox-primary">
										<input type="checkbox" id="checkbox"></input>
										<span className="checkbox"></span>
									</label>
									<div className="captions">Recordarme</div>
								</div>
								<div className="col-xs-10 offset-xs-1">
									<button type="submit" className="btn btn-primary btn-md btn-block waves-effect waves-light m-b-20">Registrarme</button>
								</div>
								<div className="row">
									<div className="col-xs-12 text-center">
										<span className="text-muted">Ya tienes una cuenta?</span>
										<Link to={"/"} className="f-w-600 p-l-5">
											Iniciar sesión
										</Link>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
	</section>
    </div>
    )
}

export default Register;