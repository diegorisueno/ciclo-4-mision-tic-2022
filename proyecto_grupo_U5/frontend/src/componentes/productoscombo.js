import axios from 'axios';
import React, {useEffect, useState} from 'react';

function ProductosCombo()
{
    /*
    const token = localStorage.getItem("token");
    let bearer;
    if (token === "") {
        bearer = "";
    } else {
        bearer = `${token}`;
    }
    const config = {
        headers: {
            'Content-Type': 'application/json', 
            'x-auth-token': bearer}
    }
    */
    const[dataCombo, setdataProductos] = useState([]);
    //Peticion GET para listar productos utilizando axios
    useEffect(()=>{axios.get('api/productos/listarcombo').then(res => {
        console.log(res.data)
        setdataProductos(res.data)
    }).catch(err=>{console.log(err)})
    },[]);
    
    return(
        <div className="form-group col-md-12">
            <select id="id_producto" className="form-control">
            {
                dataCombo.map((miproducto) => (
                <option value={miproducto.id}>{miproducto.nombre}</option>
                ))
            }
            </select>
        </div>
    )

}

export default ProductosCombo;

