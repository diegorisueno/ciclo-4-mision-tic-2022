import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

import Encabezado from './encabezado';
import MenuLateral from './menulateral';
import ClientesCombo from './clientescombo';

/*
import Checkbox from '@material-ui/core/Checkbox';
export default function Checkboxes()
{
    const [checked, setChecked] = React.useState(true);
    const handleChange = (event) => {
    setChecked(event.target.checked);
};
<Checkbox checked={checked} onChange={handleChange} inputProps={{ 'aria-label': 'primary checkbox' }}/>
*/
function PedidosEditar()
{
    const parametros = useParams()
    const[id_cliente, setIdCliente] = useState('')
    const[fecha, setFecha] = useState('')
    const[valor, setValor] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()

    const token = localStorage.getItem("token");
    let bearer;
    if(token===""){bearer="";}else{bearer=`${token}`;}
    const config={headers:{'Content-Type': 'application/json','x-auth-token': bearer}}

    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/pedidos/cargar/${parametros.id}`,config).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataPedidos = res.data[0]
        setIdCliente(dataPedidos.id_cliente)
        setFecha(dataPedidos.fecha)
        setValor(dataPedidos.valor)
        setActivo(dataPedidos.activo)
        })
    }, [])

    function pedidosActualizar()
    {
        const pedidoactualizar = {
            id: parametros.id,
            id_cliente: id_cliente,
            fecha: fecha,
            valor: valor,
            activo: activo
        }

        console.log(pedidoactualizar)

        const config = {
            body: JSON.stringify(pedidoactualizar),
            headers: {'Content-Type': 'application/json', 'x-auth-token': bearer}
        }

        axios.post(`/api/pedidos/editar/${parametros.id}`,pedidoactualizar,config).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/pedidoslistar')
            })
            .catch(err => {console.log(err)})
    
    }

    function pedidosRegresar()
    {
        //window.location.href="/";
        navigate('/pedidoslistar')
    }

    return(
            <h1>Prueba</h1>
    )

}

/*
<div className="sidebar-mini fixed">
<div className="wrapper">
    <Encabezado/>
    <MenuLateral/>
    <ClientesCombo/>

</div>
</div>
*/

/*
            <div className="row">
                <div className="col-lg-6">
                <div className="card">
                    <div className="card-header">
                        <h5 className="card-header-text">Editar Pedidos</h5>
                        <div className="f-right">
                            <a href="" data-toggle="modal" data-target="#basic-form-Modal"><i className="icofont icofont-code-alt"></i></a>
                        </div>
                    </div>
                    
                    <div className="card-block">
                        <form>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail" className="form-control-label">Email address</label>
                                <input type="email" className="form-control" id="exampleInputEmail" placeholder="Enter email"></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword" className="form-control-label">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword" placeholder="Password"></input>
                            </div>
                            <button type="submit" className="btn btn-success waves-effect waves-light m-r-30">Sign in</button>
                        </form>
                    </div>
                </div>
                </div>
            </div>
*/


/*
<div className="container mt-5">
                <h4>Pedido</h4>
                <div className="row">
                    <div className="col-md-12">
                    <div className="mb-3">
                            <label htmlFor="id_cliente" className="form-label">Id Cliente</label>
                            <input type="text" className="form-control" id="id_cliente" value={id_cliente} onChange={(e) => {setIdCliente(e.target.value)}}></input>
                        </div>                    
                        <div className="mb-3">
                            <label htmlFor="fecha" className="form-label">Fecha</label>
                            <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="valor" className="form-label">Valor</label>
                            <input type="text" className="form-control" id="valor" value={valor} onChange={(e) => {setValor(e.target.value)}}></input>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="activo" className="form-label">Activo</label>
                            <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                        </div>                
                        <div className="mb-12">
                            <button type="button" onClick={pedidosRegresar} className="btn btn-primary">Atras</button>
                            <button type="button" onClick={pedidosActualizar} className="btn btn-success">Actualizar</button>
                        </div>
                    </div>
                </div>
            </div>
*/

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default PedidosEditar;