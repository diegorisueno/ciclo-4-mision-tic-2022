import axios from 'axios';
import React, {useEffect, useState} from 'react';

//Metodo que contiene las tareas para listar pedidos
function ClientesCombo()
{

    const[dataClientes, setdataClientes] = useState([])
    //********************************************************************************
    //Codigo para enviar peticion con autenticación
    //********************************************************************************
    const token = localStorage.getItem("token");
    let bearer;
    if(token===""){bearer="";}else{bearer=`${token}`;}
    const config={headers:{'Content-Type': 'application/json','x-auth-token': bearer}}
    //********************************************************************************

    useEffect(()=>{
        axios.get('/api/clientes/listar',config).then(res => {
        console.log(res.data)
        setdataClientes(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="form-group">
            <select id="id_producto" className="form-control-label">
            {
                dataClientes.map((micliente) => (
                <option value={micliente.id}>{micliente.nombre}</option>
                ))
            }
            </select>
        </div>
    )
}

export default ClientesCombo;