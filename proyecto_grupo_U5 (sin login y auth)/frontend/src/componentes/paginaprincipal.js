import React from 'react';

function PaginaPrincipal()
{

    return(
        <div className="container mt-5">
            <h4>Pagina Principal</h4>
            <div className="row">
                <div className="col-md-12">
                    Seleccione una opción del menú, por ejemplo Productos. 
                    En esta página puede ubicar lo que desee al inicio de su aplicacion
                </div>
            </div>
        </div>
    )

}

export default PaginaPrincipal;