import {Link} from 'react-router-dom';

//Metodo que contiene las tareas para listar pedidos
function MenuLateral()
{

    return(
            <aside className="main-sidebar hidden-print ">
                <section className="sidebar" id="sidebar-scroll">
                    <ul className="sidebar-menu">
                        <li className="nav-level">Principal</li>
                        <li className="treeview">
                            <a className="waves-effect waves-dark" href="index.html">
                                <i className="icon-speedometer"></i><span>Inicio</span>
                            </a>                
                        </li>
                        <li className="nav-level">Procesos</li>
                        <li className="treeview"><a className="waves-effect waves-dark" href="#!"><i className="icon-briefcase"></i><span>Pedidos</span><i className="icon-arrow-down"></i></a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="accordion.html"><i className="icon-arrow-right"></i>Listar Pedidos</a></li>
                                <li><a className="waves-effect waves-dark" href="button.html"><i className="icon-arrow-right"></i>Agregar Pedidos</a></li>
                            </ul>
                        </li>

                        <li className="nav-level">Clientes</li>
                        <li className="treeview"><a className="waves-effect waves-dark" href="#!"><i className="icon-docs"></i><span>Clientes</span><i className="icon-arrow-down"></i></a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="accordion.html"><i className="icon-arrow-right"></i>Listar Clientes</a></li>
                                <li><a className="waves-effect waves-dark" href="button.html"><i className="icon-arrow-right"></i>Agregar Clientes</a></li>
                            </ul>
                        </li>
                </ul>
                </section>
            </aside>

    )

}

export default MenuLateral;

